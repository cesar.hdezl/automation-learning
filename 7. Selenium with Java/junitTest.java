package junitTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import pages.ConfirmationPage;
import pages.FormPage;

public class junitTest {
	
	@Test
	public void testAfterSubmit() {
		
		System.setProperty("webdriver.edge.driver","C:\\Users\\cesar\\Downloads\\msedgedriver.exe");
		WebDriver driver = new EdgeDriver();

		driver.get("https://formy-project.herokuapp.com/form");
		
		FormPage formPage = new FormPage(); 
		formPage.submitForm(driver);
		
		ConfirmationPage confirmationPage = new ConfirmationPage();
		confirmationPage.waitForAlertBanner(driver);
		
		assertEquals("Thanks for submitting your form", 
				confirmationPage.confirmSubmit(driver));

	}

}
