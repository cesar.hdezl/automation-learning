package junitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.SubmissionPage;
import pages.WebFormPage;
import pages.ReadExcel;

class ExcelTest2 {

	@FixMethodOrder(MethodSorters.DEFAULT)
	
	public class TestCases {
		
		WebDriver driver;
		SubmissionPage submissionPage;
		WebFormPage formPage;
		ReadExcel openxls;
		Date currentDate;
		
		@Before
		public void setup() {
			
			System.setProperty("webdriver.edge.driver","C:\\Users\\cesar\\Downloads\\msedgedriver.exe");
			WebDriver driver = new EdgeDriver();
			driver.get("https://formy-project.herokuapp.com/form");
		
		}
		

		@Test
		public void FormTest() throws IOException {
			
			String ssfilename = currentDate.toString().replace(" ", "-").replace(":", "-");
			// .replace(old Char, new Char)
			openxls = new ReadExcel("C:\\Users\\cesar\\Documents\\SELENIUM\\TestClean\\screenshot");

			String fname = openxls.getCellData("Hoja1", 1, 1);
			String lname = openxls.getCellData("Hoja1", 1, 2);
			String job = openxls.getCellData("Hoja1", 1, 3);
			String ed = openxls.getCellData("Hoja1", 1, 4);
			String sx = openxls.getCellData("Hoja1", 1, 5);
			int exp = openxls.getCellData("Hoja1", 1, 6);
			String datestr = openxls.getCellData("DataSheet", 1, 7);
			
			formPage = new WebFormPage(driver);
			
			formPage.setFirstname(fname);
			formPage.setLastname(lname);
			formPage.setJobTitle(job);
			formPage.setEducation(ed); //College
			File ssfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(ssfile, new File(".//screenshot//" + ssfilename + ".png"));
			
			formPage.setSex(sx); //Male
			String ssfilename2 = currentDate.toString().replace(" ", "-").replace(":", "-");			
			FileUtils.copyFile(ssfile, new File(".//screenshot//" + ssfilename2 + ".png"));
			
			formPage.setExperienceYears(exp);
			formPage.setDateString(datestr);
			String ssfilename3 = currentDate.toString().replace(" ", "-").replace(":", "-");			
			FileUtils.copyFile(ssfile, new File(".//screenshot//" + ssfilename3 + ".png"));
			
			
			assertTrue(formPage.submit());
			
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
			submissionPage = new SubmissionPage(driver);
			
			String ssfilename4 = currentDate.toString().replace(" ", "-").replace(":", "-");			
			FileUtils.copyFile(ssfile, new File(".//screenshot//" + ssfilename4 + ".png"));
			
			assertEquals("The form was successfully submitted!", 
					submissionPage.getSubmissionMessage());
			
		}
		
		@After
		public void quit() {
			driver.close();
		}

	}

}
