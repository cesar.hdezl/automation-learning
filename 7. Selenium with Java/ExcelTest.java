package junitTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.Duration;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.SubmissionPage;
import pages.WebFormPage;
import pages.ReadExcel;

class ExcelTest {

	@FixMethodOrder(MethodSorters.DEFAULT)
	public class TestCases {
		
		WebDriver driver;
		SubmissionPage submissionPage;
		WebFormPage formPage;
		ReadExcel openxls;
		
		@Before
		public void setup() {
			
			System.setProperty("webdriver.edge.driver","C:\\Users\\cesar\\Downloads\\msedgedriver.exe");
			WebDriver driver = new EdgeDriver();
			driver.get("https://formy-project.herokuapp.com/form");
		
		}
		
		@Test
		public void FormTest() {
			
			openxls = new ReadExcel("C:\\Users\\cesar\\Documents\\SELENIUM\\Excel\\LoginTestData.xlsx");

			String fname = openxls.getCellData("DataSheet", 1, 1);
			String lname = openxls.getCellData("DataSheet", 1, 2);
			String job = openxls.getCellData("DataSheet", 1, 3);
			String datestr = openxls.getCellData("DataSheet", 1, 6);
			
			formPage = new WebFormPage(driver);
			
			formPage.setFirstname(fname);
			formPage.setLastname(lname);
			formPage.setJobTitle(job);
			formPage.setEducation("c"); //College
			formPage.setSex("m"); //Male
			formPage.setExperienceYears(0);
			formPage.setDateString(datestr);
			
			assertTrue(formPage.submit());
			
			WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(4));
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div[role='alert']")));
			submissionPage = new SubmissionPage(driver);
			assertEquals("The form was successfully submitted!", 
					submissionPage.getSubmissionMessage());
			
		}
		
		@After
		public void quit() {
			driver.close();
		}

	}

}
