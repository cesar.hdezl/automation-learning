import java.util.Scanner;

public class Exercise2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		division();
		concatstring();
		
	}
		
	public static void division(){
	
		double dividend, divisor;
		
		Scanner div1 = new Scanner(System.in);
		System.out.println("Introduce dividend: ");
		dividend = div1.nextDouble();
		
		Scanner div2 = new Scanner(System.in);
		System.out.println("Introduce divisor: ");
		divisor = div2.nextDouble();
		
		try {
			
			double res = dividend / divisor;
			System.out.println("Result is: " + res);
			
		}
		
		catch (Exception e) {
			
			System.out.println("Result is: Infinity");
			
		}
		
		return;
	}
	
	public static void concatstring() {
		
		String str1 = null;
		String str2 = "hello";
		
		try {
			
			System.out.println("\n" + str1 + str2);
			
		}
		
		catch (Exception e) {
			
			System.out.println(e);
			
		}
		
	}
		
}


