
public class Exercise1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		FibonacciNum(5);
		
	}

	public static void FibonacciNum(long lim) {

		long initnum = 0;
		long newnum = 1;
		long res = 1;

		for(int N = 1; N <= lim; N++) {

			res = initnum + newnum;
			initnum = newnum;
			newnum = res;
			System.out.println(res);

		}

	}
		
}
