import java.text.ParseException;

public class StudList {
	
	public static void main(String[] args) throws ParseException {
		
        StudList stud1 = new StudList("Cesar", "Alejandro", "Hernandez", "01-01-1996");
        StudList stud2 = new StudList("Juan", "Antonio", "Reyes", "22-07-1988");

        System.out.println(stud1.name() + ". " + stud1.DOB());
        System.out.println(stud2.name() + ". " + stud2.DOB());
       
    }
	
private String fname;
    private String mname;
    private String lname;
    private String DateBirth;
    private int ID;
	
	public StudList(String fname, String mname, String lname, String DateBirth){
        this.fname = fname;
        this.mname = mname;
        this.lname = lname;
        this.DateBirth = DateBirth;
    }

	public String FirstName() {
        return fname;
    }

    public void setFirstName(String value) {
        this.fname = value;
    }
	
    public String MiddleName() {
        return mname;
    }

    public void setMiddleName(String value) {
        this.mname = value;
    }

    public String LastName() {
        return lname;
    }

    public void setLastName(String value) {
        this.lname = value;
    }

    
    public String name(){
        return fname + " " +  mname + " " + lname;
    }
    
    
    
    public int StudID() {
        return ID;
    }

    public void setStudID(int value) {
        this.ID = value;
    }

    
    public String DOB() throws ParseException {
        String date = DateBirth;
        String[] dates = date.split("-");
        String newDate = String.format("%s-%s-%s", dates[2], dates[1], dates[0]);
        this.DateBirth = newDate;
        return DateBirth;
    }

    public void setDOB(String value) {
        this.DateBirth = value;
    }

}
