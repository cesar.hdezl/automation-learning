package pages;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Purchase {
	
	WebDriver driver;
	private static final Logger logger = LogManager.getLogger(Purchase.class);
	public String searchfilter;

	
	//WebElement
	WebElement searchfilterElem;
	WebElement addBackpack;
	WebElement addBikelight;
	WebElement addTShirt;
	WebElement addJacket;
	WebElement addOnesie;
	WebElement addredTShirt;

	public Purchase(WebDriver driver) {
		
		this.driver = driver;
		searchfilterElem = driver.findElement(By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select"));
		addBackpack = driver.findElement(By.cssSelector("#add-to-cart-sauce-labs-backpack"));
		addBikelight = driver.findElement(By.id("add-to-cart-sauce-labs-bike-light"));
		addTShirt = driver.findElement(By.cssSelector("#add-to-cart-sauce-labs-bolt-t-shirt"));
		addJacket = driver.findElement(By.cssSelector("#add-to-cart-sauce-labs-fleece-jacket"));
		addOnesie = driver.findElement(By.cssSelector("#add-to-cart-sauce-labs-onesie"));
		addredTShirt = driver.findElement(By.cssSelector("#add-to-cart-test\\.allthethings\\(\\)-t-shirt-\\(red\\)"));
		
	}
	
	public String getSearchFilter(WebDriver driver) {
		return searchfilter;
	}
	
	public void setSearchFilter(WebDriver driver, String searchfilter) {
		this.searchfilter = searchfilter;
		Select filterSelect = new Select(searchfilterElem);
		
		if(searchfilter.equalsIgnoreCase("Descendent Name")) {
			filterSelect.selectByValue("az");
			String az = driver.findElement(By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[1]")).getText();
			logger.info("{} sorting is selected.", az);
		}
		else if(searchfilter.equalsIgnoreCase("Ascendent Name")) {
			filterSelect.selectByValue("za");
			String za = driver.findElement(By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[2]")).getText();
			logger.info("{} sorting is selected.", za);
		}
		else if(searchfilter.equalsIgnoreCase("Ascendent Price")) {
			filterSelect.selectByValue("lohi");
			String lohi = driver.findElement(By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[3]")).getText();
			logger.info("{} sorting is selected.", lohi);
		}
		else if(searchfilter.equalsIgnoreCase("Descendent Price")) {
			filterSelect.selectByValue("hilo");
			String hilo = driver.findElement(By.xpath("//*[@id=\"header_container\"]/div[2]/div[2]/span/select/option[4]")).getText();
			logger.info("{} sorting is selected.", hilo);
		}
		
	}
	
	public void addBackpack(WebDriver driver) {
		String backpackTitle = driver.findElement(By.id("item_4_title_link")).getText();
		logger.info("{} adding to the Cart.", backpackTitle);
		addBackpack.click();
		logger.info("{} is added to the Cart.", backpackTitle);
	}
	
	public void addBikeLight(WebDriver driver) {
		String bikelightTitle = driver.findElement(By.id("item_0_title_link")).getText();
		logger.info("{} adding to the Cart.", bikelightTitle);
		addBikelight.click();
		logger.info("{} is added to the Cart.", bikelightTitle);
	}
	
	public void addTShirt(WebDriver driver) {
		String tshirtTitle = driver.findElement(By.id("item_1_title_link")).getText();
		logger.info("{} adding to the Cart.", tshirtTitle);
		addTShirt.click();
		logger.info("{} is added to the Cart.", tshirtTitle);
	}
	
	public void addJacket(WebDriver driver) {
		String jacketTitle = driver.findElement(By.id("item_5_title_link")).getText();
		logger.info("{} adding to the Cart.", jacketTitle);
		addJacket.click();
		logger.info("{} is added to the Cart.", jacketTitle);
	}
	
	public void addOnesie(WebDriver driver) {
		String onesieTitle = driver.findElement(By.id("item_2_title_link")).getText();
		logger.info("{} adding to the Cart.", onesieTitle);
		addOnesie.click();
		logger.info("{} is added to the Cart.", onesieTitle);
	}
	
	public void addRedTShirt(WebDriver driver) {
		String redtshirtTitle = driver.findElement(By.id("item_3_title_link")).getText();
		logger.info("{} adding to the Cart.", redtshirtTitle);
		addredTShirt.click();
		logger.info("{} is added to the Cart.", redtshirtTitle);
	}
	
	public boolean Finish() {
		WebElement finish = driver.findElement(By.id("finish"));
		String finishedText = finish.getText();
		finish.click();
		logger.info("Clicking {}", finishedText);
		return true;
	}
	
	public boolean backHome(WebDriver driver) {
		String confirm = driver.findElement(By.cssSelector("#checkout_complete_container > h2")).getText();
		logger.info("{}", confirm);
		driver.findElement(By.id("back-to-products")).click();
		return true;
	}
	
	public void screenshot(WebDriver driver, String ssfilename) throws IOException {
		logger.info("Taking screenshot");
		File ssfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ssfile, new File(".//screenshot//" + ssfilename + ".png"));
	}
}
