package pages;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Cart {
	
	WebDriver driver;
	private static final Logger logger = LogManager.getLogger(Cart.class);
	
	public Cart(WebDriver driver) {
		this.driver = driver;
	}

	public boolean checkCart() {
		driver.findElement(By.className("shopping_cart_link")).click();
		String carturl = "https://www.saucedemo.com/cart.html";
		logger.info("Navigating to the Cart list: {}", carturl);
		return true;
	}
	
	public boolean clickOnCheckout() {
		WebElement checkout = driver.findElement(By.id("checkout"));
		String checkoutText = checkout.getText();
		logger.info("Clicking on {}", checkoutText);
		checkout.click();
		return true;
	}
	
	public void screenshot(WebDriver driver, String ssfilename) throws IOException {
		logger.info("Taking screenshot");
		File ssfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ssfile, new File(".//screenshot//" + ssfilename + ".png"));
	}
	
}
