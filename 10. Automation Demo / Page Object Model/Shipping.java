package pages;

import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Shipping {
	
	WebDriver driver;
	private static final Logger logger = LogManager.getLogger(Shipping.class);
	
	private String fname;
	private String lname;
	private String zip;
	
	//WebElements
	WebElement fnameElem;
	WebElement lnameElem;
	WebElement zipElem;
	WebElement continueElem;
	
	public Shipping(WebDriver driver) {
		this.driver = driver;
		fnameElem = driver.findElement(By.cssSelector("#first-name"));
		lnameElem = driver.findElement(By.cssSelector("#last-name"));
		zipElem = driver.findElement(By.cssSelector("#postal-code"));
		continueElem = driver.findElement(By.cssSelector("#continue"));
	}
	
	public String getFirstName() {
		return fname;
	}
	
	public void setFirstNameForShipping(WebDriver driver, String fname) {
		this.fname = fname;	
		fnameElem.sendKeys(fname);
		logger.info("First name: {}", fname);
	}
	
	public String getLastName() {
		return lname;
	}
	
	public void setLastNameForShipping(WebDriver driver, String lname) {
		this.lname = lname;
		lnameElem.sendKeys(lname);
		logger.info("Last name: {}", lname);

	}
	
	public String getZip() {
		return zip;
	}
	
	public void setZipForShipping(WebDriver driver, String zip) {
		this.zip = zip;
		zipElem.sendKeys(zip);
		logger.info("Zip / Postal Code: {}", zip);

	}
	
	public boolean Continue() {
		continueElem.click();
		return true;
	}
	
	public void DataMissing(WebDriver driver) {
		String missing = driver.findElement(By.xpath("//*[@id=\"checkout_info_container\"]/div/form/div[1]/div[4]"))
				.getText();
		logger.info("{}", missing);
	}
	
	public void getPaymentInformation() {
		String payInfo = driver.findElement(By.xpath("//*[@id=\"checkout_summary_container\"]/div/div[2]/div[1]")).getText();
		String cardInfo = driver.findElement(By.xpath("//*[@id=\"checkout_summary_container\"]/div/div[2]/div[2]")).getText();
		logger.info("{} {}", payInfo, cardInfo);
	}
	
	public void getShippingInformation() {
		String shipInfo = driver.findElement(By.xpath("//*[@id=\"checkout_summary_container\"]/div/div[2]/div[3]")).getText();
		String deliveryInfo = driver.findElement(By.xpath("//*[@id=\"checkout_summary_container\"]/div/div[2]/div[4]")).getText();
		logger.info("{} {}", shipInfo, deliveryInfo);
	}
	
	public void getTotalPrice() {
		String total = driver.findElement(By.xpath("//*[@id=\"checkout_summary_container\"]/div/div[2]/div[7]")).getText();
		logger.info("{}", total);
	}
	
	public void screenshot(WebDriver driver, String ssfilename) throws IOException {
		logger.info("Taking screenshot");
		File ssfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ssfile, new File(".//screenshot//" + ssfilename + ".png"));
	}

}
