package pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Logout {
	
	WebDriver driver;
	private static final Logger logger = LogManager.getLogger(Logout.class);
	
	public Logout(WebDriver driver) {
		this.driver = driver;
	}

	public void logOut() throws InterruptedException {
		logger.info("Logging out...");
		driver.findElement(By.xpath("//*[@id=\"menu_button_container\"]/div/div[1]/div")).click();
		logger.info("Logged Out");
		driver.findElement(By.cssSelector("#logout_sidebar_link")).click();
		
	}
	
}
