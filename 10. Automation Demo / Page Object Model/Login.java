package pages;

import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login {

	private static final Logger logger = LogManager.getLogger(Login.class);
	
	WebDriver driver;
	static Date currentDate;
	public String userName;
	public String pswd;
	

	// WebElements
	WebElement usernameElem;
	WebElement pswdElem;
	WebElement signinElem;
	
	public Login(WebDriver driver) {
		this.driver = driver;
		usernameElem = driver.findElement(By.id("user-name"));
		pswdElem = driver.findElement(By.id("password"));
		signinElem  = driver.findElement(By.id("login-button"));
	}
	
	public String getUser() {
		return userName;
	}

	public void setUser(WebDriver driver, String userName) {

		this.userName = userName;
		
		logger.info("Entering an accepted Username and Password");
		
		// Obtaining Accepted Usernames
		String users = driver.findElement(By.xpath("//*[@id=\"login_credentials\"]")).getText();
		//There are break lines <br> in the element 
		String[] usernames = users.split("\n"); 
		String user1 = usernames[1]; //standard_user
		String user2 = usernames[2]; //locked_out_user
		String user3 = usernames[3]; //problem_user
		String user4 = usernames[4]; //performance_glitch_user
		String user5 = "standard_user1";
		
		if(userName.equalsIgnoreCase("user1")) {
			usernameElem.sendKeys(user1);
			logger.info("Username: {}", user1);
		}
		else if(userName.equalsIgnoreCase("user2")) {
			usernameElem.sendKeys(user2);
			logger.info("Username: {}", user2);
		}
		else if(userName.equalsIgnoreCase("user3")) {
			usernameElem.sendKeys(user3);
			logger.info("Username: {}", user3);
		}
		else if(userName.equalsIgnoreCase("user4")) {
			usernameElem.sendKeys(user4);
			logger.info("Username: {}", user4);
		}
		else if(userName.equalsIgnoreCase("user5")) {
			usernameElem.sendKeys(user5);
			logger.info("Username: {}", user5);
		}
		
	}
	
	public String getPassword() {
		return pswd;
	}
	
	public void setPassword(WebDriver driver, String pswd) {

		this.pswd = pswd;
		String pswdText = driver.findElement(By.xpath("//div[@class=\"login_password\"]")).getText();
		//There are break lines <br> in the element
		String[] password = pswdText.split("\n");
		String pswd1 = password[1];
		String pswd2 = "secret_sauce1";
		
		if(pswd.equalsIgnoreCase("pswd1")) {
			pswdElem.sendKeys(pswd1);
			logger.info("Password: {}", pswd1);
		}
		else if(pswd.equalsIgnoreCase("pswd2")) {
			pswdElem.sendKeys(pswd2);
			logger.info("Password: {}", pswd2);
		}
		
	}
	
	public boolean logIn(WebDriver driver) {
		logger.info("Logging...");
		signinElem.click();
		return true;	
	}
	
	public String verifyLogin(WebDriver driver) {
		return driver.findElement(By.xpath("//*[@id=\"header_container\"]/div[2]/span")).getText();
		
	}
	
	public void verifyLockedAccount(WebDriver driver) {
		String locked = driver.findElement(By.xpath("//*[@id=\"login_button_container\"]/div/form/div[3]"))
				.getText();
		logger.info("{}", locked);
	}
	
	public void incorrectData(WebDriver driver) {
		String incorrect = driver.findElement(By.xpath("//*[@id=\"login_button_container\"]/div/form/div[3]"))
				.getText();
		logger.info("{}", incorrect);
	}
	
	public void screenshot(WebDriver driver, String ssfilename) throws IOException {
		logger.info("Taking screenshot");
		File ssfile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(ssfile, new File(".//screenshot//" + ssfilename + ".png"));
	}
	
}
