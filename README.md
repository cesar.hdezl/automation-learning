# Automation Learning
This is an Automation Learning Workshop at Tata Consultancy Services

# Assignments/Projects
1. Using GitLab
2. Scrum Overview and Jira Setup
3. Java
4. JUnit / Assertion / log4j
5. HTML
6. XPath Overview
7. Selenium with Java
8. Automation Framework
9. Software Testing Overview
10. Presentation and Automation Demo
11. Automation Interview
